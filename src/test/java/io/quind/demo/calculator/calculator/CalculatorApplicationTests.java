package io.quind.demo.calculator.calculator;

import io.quind.demo.calculator.calculator.model.Model.ModelResult;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CalculatorApplicationTests {

	@Test
	void contextLoads() {
		float number1 = 4;
		float number2 = 3;
		float prediction = 12;
		float result=0;

		ModelResult testResult = new ModelResult();
		result=testResult.setResult(number1*number2);

		assertEquals(result,prediction);


	}

}
