package io.quind.demo.calculator.calculator.model.Model;

import org.springframework.scheduling.support.SimpleTriggerContext;

public class ModelResult {
    private float result;
    private String Status;

    public float getResult(){
        return result;
    }
    public float setResult(Float result){
        this.result=result;
        return result;
    }

    public void setResult(String status) {
       Status = status;
    }

    public String getStatus() {
        return Status;
    }
    public String setStatus(String status){
        this.Status=status;
        return status;

    }

}
