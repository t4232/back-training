package io.quind.demo.calculator.calculator.service;

import io.quind.demo.calculator.calculator.model.Model.ModelResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.lang.Exception;

@RestController
    @CrossOrigin(origins = ("*"))
    public class CalculatorService {
    ModelResult obj1 = new ModelResult();

    @RequestMapping("/suma")
    public ModelResult get(@RequestParam(name = "num1", defaultValue = "0") Float n1,
                           @RequestParam(name = "num2", defaultValue = "0") Float n2) {
        obj1.setResult((n1 + n2));
        obj1.setStatus("ok");
        return obj1;
    }

    @RequestMapping("/resta")
    public ModelResult get1(@RequestParam(name = "num1", defaultValue = "0") Float n1,
                            @RequestParam(name = "num2", defaultValue = "0") Float n2) {
        obj1.setResult((n1 - n2));
        obj1.setStatus("ok");
        return obj1;
    }

    @RequestMapping("/mult")
    public ModelResult get2(@RequestParam(name = "num1", defaultValue = "0") Float n1,
                            @RequestParam(name = "num2", defaultValue = "0") Float n2) {
        obj1.setResult((n1 * n2));
        obj1.setStatus("ok");
        return obj1;
    }

    @RequestMapping("/divi")
    public ModelResult get3(@RequestParam(name = "num1", defaultValue = "0") Float n1,
                            @RequestParam(name = "num2", defaultValue = "0") Float n2) {
        try {
            obj1.setResult(n1 / n2);
            obj1.setStatus("ok");
            return obj1;
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return null;
    }
}



